import styled from 'styled-components'
import Image from 'next/image'
import React, { useEffect } from 'react'

type DialogProps = {
  title?: string
  isOpen: boolean
  onClose: () => void
  closeOnOverlayClick: boolean
  children?: React.ReactNode
}

const Dialog = ({
  title,
  children,
  onClose,
  closeOnOverlayClick,
  isOpen
}: DialogProps) => {
  const handleOverlayClick = (event: React.MouseEvent<HTMLDivElement>) => {
    if (!closeOnOverlayClick) return null

    if (event.target === event.currentTarget) {
      onClose()
    }
  }

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        onClose()
      }
    }

    window.addEventListener('keydown', handleKeyDown)

    return () => {
      window.removeEventListener('keydown', handleKeyDown)
    }
  }, [onClose])

  if (!isOpen) return null

  return (
    <S.Dialog onClick={handleOverlayClick} data-testid="overlay">
      <S.Content data-testid="content">
        <S.Header>
          <S.Title>{title}</S.Title>
          <S.Button onClick={onClose} data-testid="close">
            <Image src="/close.svg" alt="Close" width={16} height={16} />
          </S.Button>
        </S.Header>
        {children}
      </S.Content>
    </S.Dialog>
  )
}

const S = {
  Dialog: styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.8);
    z-index: 1000;
  `,
  Content: styled.div`
    width: 80%;
    height: 80%;
    padding: 20px;
    background-color: white;
    overflow-y: auto;
  `,
  Header: styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-bottom: 24px;
  `,
  Title: styled.h2`
    font-size: 22px;
    font-weight: 600;
  `,
  Button: styled.button`
    background-color: transparent;
    border: none;
    cursor: pointer;
  `
}

export default Dialog
