import { createGlobalStyle } from 'styled-components'
import reset from './reset'

const GlobalStyle = createGlobalStyle`
  ${reset}
  * {
    box-sizing: border-box;
  }
  html {
    font-family: 'Inter', sans-serif;
  }
  a {
    text-decoration: none;
    color: inherit;
  }
`

export default GlobalStyle
