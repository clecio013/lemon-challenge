describe('Home', () => {
  beforeEach(() => {
    cy.lemon()
  })

  it('should visit home page and open dialog with correct data', () => {
    cy.findByText('Open Dialog').click()
    cy.get('h2').should('contain', 'Title')
  })

  it('should close modal when click on close button', () => {
    cy.findByText('Open Dialog').click()
    cy.findByTestId('close').click()
    cy.get('h2').should('not.exist')
  })

  it('should close modal when press ESC', () => {
    cy.findByText('Open Dialog').click()
    cy.get('body').type('{esc}')
    cy.get('h2').should('not.exist')
  })

  it('should close modal when click outside', () => {
    cy.findByText('Open Dialog').click()
    cy.get('body').click(0, 0)
    cy.get('h2').should('not.exist')
  })
})
