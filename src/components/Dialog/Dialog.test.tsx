import { render, screen } from '@testing-library/react'
import Dialog from './Dialog'

describe('Dialog', () => {
  it('should render the Dialog', () => {
    render(
      <Dialog
        title="Title"
        onClose={() => {}}
        isOpen={true}
        closeOnOverlayClick
      >
        Hello
      </Dialog>
    )
    expect(screen.getByText('Title')).toBeInTheDocument()
    expect(screen.getByText('Hello')).toBeInTheDocument()
  })

  it('should not render the Dialog', () => {
    render(
      <Dialog
        title="Title"
        onClose={() => {}}
        isOpen={false}
        closeOnOverlayClick
      >
        Hello
      </Dialog>
    )
    expect(screen.queryByText('Title')).not.toBeInTheDocument()
    expect(screen.queryByText('Hello')).not.toBeInTheDocument()
  })

  it('should call onClose when clicking on the close button', () => {
    const onClose = jest.fn()
    render(
      <Dialog title="Title" onClose={onClose} isOpen={true} closeOnOverlayClick>
        Hello
      </Dialog>
    )
    screen.getByTestId('close').click()
    expect(onClose).toHaveBeenCalled()
  })

  it('should call onClose when clicking on the overlay', () => {
    const onClose = jest.fn()
    render(
      <Dialog title="Title" onClose={onClose} isOpen={true} closeOnOverlayClick>
        Hello
      </Dialog>
    )
    screen.getByTestId('overlay').click()
    expect(onClose).toHaveBeenCalled()
  })

  it('should not call onClose when clicking on the content', () => {
    const onClose = jest.fn()
    render(
      <Dialog title="Title" onClose={onClose} isOpen={true} closeOnOverlayClick>
        Hello
      </Dialog>
    )
    screen.getByText('Hello').click()
    expect(onClose).not.toHaveBeenCalled()
  })

  it('should call onClose when pressing the Escape key', () => {
    const onClose = jest.fn()
    render(
      <Dialog title="Title" onClose={onClose} isOpen={true} closeOnOverlayClick>
        Hello
      </Dialog>
    )
    global.window.dispatchEvent(new KeyboardEvent('keydown', { key: 'Escape' }))
    expect(onClose).toHaveBeenCalled()
  })
})
