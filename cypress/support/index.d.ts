// load type definitions from Cypress module
/// <reference types="Cypress" />

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to visit google page
     * @example cy.google()
     */
    lemon(): Chainable<Window>
  }
}
