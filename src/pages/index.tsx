import { useState } from 'react'
import Dialog from 'components/Dialog'
import styled from 'styled-components'

const Home = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false)

  const handleToggleDialog = () => {
    setIsDialogOpen(!isDialogOpen)
  }

  return (
    <S.Home>
      <S.Button onClick={handleToggleDialog}>Open Dialog</S.Button>
      <Dialog
        title="Title"
        onClose={handleToggleDialog}
        isOpen={isDialogOpen}
        closeOnOverlayClick
      >
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum
          dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
          commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
          velit esse cillum dolore eu fugiat nulla pariatur.
        </p>
      </Dialog>
    </S.Home>
  )
}

const S = {
  Home: styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
  `,
  Button: styled.button`
    border: none;
    font-size: 16px;
    padding: 12px 16px;
    border-radius: 4px;
    background-color: rgba(0, 128, 89, 0.8);
    color: #fff;
    transition: 0.1s;
    cursor: pointer;

    &:hover {
      background-color: rgba(0, 128, 89, 1);
    }
  `
}

export default Home
